package com.epam;

import com.epam.productsxmlschema.Catalog;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Catalog catalog = (Catalog) unmarshaller.unmarshal(new FileInputStream("catalog.xml"));

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File("catalog.json"), catalog);
            Catalog catalogJson = objectMapper.readValue(new File("catalog.json"), Catalog.class);

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(catalogJson, new FileOutputStream("catalogConverted.xml"));
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
    }
}